import asyncio

from app.internal.parser.parser import Parser


def main():
    parser = Parser()
    asyncio.run(parser.run())


if __name__ == "__main__":
    main()
