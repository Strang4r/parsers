import asyncio
from concurrent.futures import ProcessPoolExecutor
from typing import List

import aiohttp
from aiohttp import ClientSession, ServerDisconnectedError
from aiohttp_socks import ProxyConnector
from pydantic import PositiveInt, AnyUrl
from python_socks import ProxyTimeoutError

from app.internal.clients.csv_client import CsvClient
from app.internal.clients.habr_client import HabrClient
from app.internal.parser.base_parser import BaseParser
from app.pkg.settings import settings
from app.pkg.settings.logger import get_logger

logger = get_logger("Parser")


class Parser(BaseParser):
    __last_position: PositiveInt = 1
    __counter: PositiveInt = 1
    __header: dict
    __pool: ProcessPoolExecutor()
    __habr_client: HabrClient()
    __csv_client: CsvClient()

    def __init__(
            self,
            **kwargs,
    ):
        super().__init__(**kwargs)
        # self.__pool = ProcessPoolExecutor(settings.MAX_PROCESSES)
        self.__habr_client = HabrClient()
        self.__csv_client = CsvClient()

    @property
    def __header(self):
        return {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-language': 'en-US,en;q=0.8',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
        }

    async def run(self):
        urls = await self.__csv_client.get_urls()
        self.__last_position = self.__csv_client.get_last_position()
        await self.scrape_urls(urls)

    def scrape_page(self, html: str, url: AnyUrl) -> List[str]:
        try:
            data = []
            name = self.__habr_client.get_username_from_html(html)

            data.append(url)
            data.append(name)
            data.append(html)

            logger.info(self.__counter)
            self.__csv_client.write_all(data, self.__counter)
            self.__counter += 1
            self.__last_position += 1
            return data

        except RuntimeError as err:
            logger.error(f"Error occurred while scraping page : {err}", )
        except Exception as err:
            logger.error(f"Error occurred while scraping page : {err}", )

    async def start_parsing(self, session: ClientSession, url: AnyUrl):

        if self.__counter != self.__last_position:
            self.__counter += 1
            return

        html = await self.__habr_client.get_html(session, url)
        loop = asyncio.get_event_loop()
        event = await loop.run_in_executor(None, self.scrape_page, html, url)
        return event

    async def scrape_urls(self, urls: List):
        print('starting tasks')
        connector = ProxyConnector.from_url(settings.PROXY)
        try:

            async with aiohttp.ClientSession(connector=connector, headers=self.__header) as session:
                return await asyncio.gather(*(self.start_parsing(session, url) for url in urls))

        except (ProxyTimeoutError, ServerDisconnectedError) as err:
            logger.error(f"Error occurred while getting html page :{err}", )
        except Exception as err:
            logger.error(f"Unknown Error occurred :{err}", )
