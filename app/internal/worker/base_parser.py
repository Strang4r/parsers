import abc
import csv
import typing

from pydantic import PositiveInt

from app.pkg.settings.logger import get_logger

logger = get_logger("BaseWorker")


class BaseWorker:

    async def run(self):
        while True:
            try:
                await self.__run()
            except Exception:
                continue

    @abc.abstractmethod
    async def __run(self):
        raise NotImplementedError()

    async def get_urls(self) -> typing.List[str]:
        with open('data/habr.csv', "r") as file:
            reader = csv.reader(file, delimiter=",", quotechar='"')
            urls = [row[0] for row in reader]
        return urls

    async def write_to_file(self, data: typing.List[str], counter: PositiveInt) -> None:
        try:
            with open(f"data/users/{counter}.csv", "x") as f:
                f.writelines([data[0] + '\n', data[1] + '\n', data[2] + '\n'])
                logger.info(f"written to file N {counter}")
        except FileExistsError:
            logger.info(f"file {counter} already exists, skipping... ")

    async def write_last_position(self, counter: PositiveInt) -> None:
        with open('data/last_position.csv', "w") as file:
            file.write(str(counter))

    async def get_last_position(self) -> PositiveInt:
        with open("data/last_position.csv", "r") as file:
            position = file.readline()
        return int(position)
