import csv
import typing
from logging import Logger

from pydantic import PositiveInt

from app.pkg.settings.logger import get_logger


class CsvClient():
    __logger: Logger

    def __init__(self):
        self.__logger = get_logger("BaseParser")

    async def get_urls(self) -> typing.List[str]:
        with open('data/habr.csv', "r") as file:
            reader = csv.reader(file, delimiter=",", quotechar='"')
            urls = [row[0] for row in reader]
        return urls

    def write_to_file(self, data: typing.List[str], counter: PositiveInt) -> None:
        try:
            with open(f"data/users/{counter}.csv", "x", encoding="utf-8") as f:
                f.writelines([data[0] + '\n', data[1] + '\n', data[2] + '\n'])
        except FileExistsError:
            self.__logger.info(f"file {counter} already exists, skipping... ")

    def write_last_position(self, counter: PositiveInt) -> None:
        with open('data/last_position.csv', "w") as file:
            file.write(str(counter))

    def get_last_position(self) -> PositiveInt:
        with open("data/last_position.csv", "r") as file:
            position = file.readline()
        return int(position)

    def write_all(self, data: typing.List, counter: int):
        self.write_to_file(data, counter)
        self.write_last_position(counter)
