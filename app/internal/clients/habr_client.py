from logging import Logger

from aiohttp import ClientSession, ClientOSError
from bs4 import BeautifulSoup
from pydantic import AnyUrl
from python_socks import ProxyTimeoutError

from app.pkg.settings.logger import get_logger


class HabrClient():
    __logger: Logger

    def __init__(self):
        self.__logger = get_logger("HabrClient")

    def get_username_from_html(self, html: str) -> str:
        try:
            soup = BeautifulSoup(html, 'lxml')
            name = soup.find('div', class_='user-page-sidebar__title')
            return name.string

        except AttributeError:
            return 'Not Found'

    async def get_html(self, session: ClientSession, url: AnyUrl) -> str:
        for _ in range(3):
            try:
                async with session.get(url) as response:
                    if response.status != 200:
                        continue
                    return await response.text()

            except (ProxyTimeoutError, ClientOSError) as err:
                self.__logger.error(f"Error occurred while getting html page :{err}", )
            except Exception as err:
                self.__logger.error(f"Unknown error occurred while getting html page :{err}", )
