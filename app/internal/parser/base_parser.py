from app.pkg.settings.logger import get_logger

logger = get_logger("BaseParser")


class BaseParser:

    async def run(self):
        while True:
            try:
                pass
            except Exception:
                continue
