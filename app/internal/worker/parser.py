import asyncio
import time
from concurrent.futures.process import ProcessPoolExecutor
from typing import List

import aiohttp
from bs4 import BeautifulSoup
from pydantic import PositiveInt, AnyUrl

from app.internal.worker.base_parser import BaseWorker
from app.pkg.settings.logger import get_logger

logger = get_logger("Parser")


class Parser(BaseWorker):
    last_position: PositiveInt
    counter: PositiveInt
    # proxy_dict = {"html": None}
    start_time: time.time()

    _pool = ProcessPoolExecutor(12)

    def __init__(
            self,
            **kwargs,
    ):
        super().__init__(**kwargs)
        self.last_position = 1
        self.counter = 1
        self.start_time = time.time()

    async def run(self):
        while True:
            try:
                self.last_position = await self.get_last_position()
                await self.__run()
            except Exception as err:
                logger.error(f"Error occurred while parsing:{err}", )

    async def __run(self):
        urls = await self.get_urls()
        try:
            await self.scrape_urls(urls)
        except (AttributeError, TypeError, UnicodeDecodeError) as err:
            print(err)
            logger.warning(f"Error occurred while parsing: {err} ")

    async def get_html(self, session, url: AnyUrl) -> str:
        async with session.get(url) as response:
            return await response.text()

    async def get_data(self, html: str, url: AnyUrl) -> List[str]:
        data = []
        soup = BeautifulSoup(html, 'lxml')

        name = soup.find('div', class_='user-page-sidebar__title').string

        data.append(name)
        data.append(url)
        data.append(html)

        return data

    async def parse(self, session, url: AnyUrl) -> List[str]:  # get all data
        html = await self.get_html(session, url)
        return await self.get_data(html, url)

    async def parse_and_write(self, session, url: AnyUrl):
        if self.counter != self.last_position:
            self.counter += 1
            return

        if (self.counter % 100) == 0:
            speed = time.time() - self.start_time
            print(speed)

        data = await self.parse(session, url)  # get all data
        loop = asyncio.get_event_loop()
        await self.write_to_file(data, self.counter)
        self.counter += 1
        await self.write_last_position(self.counter)
        self.last_position += 1
        return await loop.run_in_executor(self._pool, self.parse, url)

    async def scrape_urls(self, urls: List):
        async with aiohttp.ClientSession() as session:
            return await asyncio.gather(*(self.parse_and_write(session, url) for url in urls))
