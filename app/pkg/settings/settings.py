import pathlib
from functools import lru_cache
from pathlib import Path

import pydantic
from dotenv import find_dotenv
from pydantic.env_settings import BaseSettings


class _Settings(BaseSettings):
    class Config:
        env_file_encoding = "utf-8"
        arbitrary_types_allowed = True


class Settings(_Settings):
    LOGGER_FILE_PATH: pathlib.Path
    LOGGER_LEVEL: str

    CSV_PATH: Path

    PROXY: pydantic.AnyUrl
    MAX_PROCESSES: int


@lru_cache()
def get_settings(env_file: str = ".env") -> Settings:
    return Settings(_env_file=find_dotenv(env_file))
