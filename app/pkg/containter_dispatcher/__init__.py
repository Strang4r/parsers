from typing import Callable, List

from dependency_injector.containers import Container



def __wire_packages(
    container: Callable[..., Container],
    pkg_name: str,
    packages: List[str],
):
    container().wire(packages=[pkg_name, *packages])


def register_container(pkg_name: str, packages: List[str] = None) -> None:

    if packages is None:
        packages = ["app.internal"]

    # __wire_packages(Workers, pkg_name=pkg_name, packages=packages)
